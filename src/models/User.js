import { Model } from 'sequelize'

const bcrypt = require('bcrypt');

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    static associate(models) {
      const { UserHistory } = models

      User.hasMany(UserHistory, { foreignKey: 'id' })
    }

    static hash = (password) => bcrypt.hashSync(password, 8)
  }

  User.init({
    id: {
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    email: DataTypes.STRING,
    fullName: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'User',
    timestamps: false,
  })

  return User
}
