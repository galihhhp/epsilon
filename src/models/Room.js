import { Model } from 'sequelize'

module.exports = (sequelize, DataTypes) => {
  class Room extends Model {
    static associate(models) {}
  }

  Room.init({
    roomNumber: {
      primaryKey: true,
      type: DataTypes.INTEGER,
    },
    firstPlayer: DataTypes.STRING,
    secondPlayer: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'Room',
    timestamps: false,
  })

  return Room
}
