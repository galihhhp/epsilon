import bcrypt from 'bcrypt'
// import fetch from 'node-fetch'
// import { cookie } from 'express-validator'
import { Admin } from '../../models'
import { JwtAuth } from '../../middlewares/Auth'

class AdminController {
  static login = (req, res) => {
    const { username, password } = req.body

    return Admin.findOne({
      where: {
        username,
      },
    }).then(
      (admin) => {
        if (admin && bcrypt.compareSync(password, admin.password)) {
          const { id } = admin

          return JwtAuth.sign({ id },
            (token) => res.status(200).cookie('token', token, { httpOnly: true }).json({ token }));
        }

        return res.status(401).json({ message: 'Unauthorized' })
      },
    ).catch(
      () => res.status(500).json({ message: 'Internal Server Error' }),
    )
  }

  static register = (req, res) => {
    const { username, password, confirmPassword } = req.body

    if (password !== confirmPassword) {
      res.status(404).json({ msg: 'please, check your input' })
    } else {
      const hashPassword = Admin.hash(password)

      Admin.create({
        username,
        password: hashPassword,
      })
        .then((admin) => res.status(200).json(admin))
        .catch((err) => res.status(500).json(err))
    }
  }

  static get = (req, res) => Admin.findAll().then(
    (admins) => res.status(200).json(admins),
  ).catch(
    (err) => res.status(400).json({ msg: err }),
  )

  static getById = (req, res) => {
    const { id } = req.params

    Admin.findOne({
      where: { id },
    })
      .then((admin) => res.status(200).json(admin))
      .catch((err) => res.status(400).json({ msg: 'ID not found' }))
  }

  static update = (req, res) => {
    const { id } = req.params
    const { username, password } = req.body

    Admin.update({
      username,
      password,
    }, {
      where: { id },
    })
      .then(() => res.json({ msg: 'Updated' }))
      .catch((err) => res.status(400).json(err))
  }

  static deleteAll = (req, res) => Admin.destroy({
    truncate: true,
    cascade: true,
  }).then(
    (admin) => {
      if (admin) return res.status(400).json({ msg: 'ID Not Found' })

      return res.status(200).json({ msg: 'Deleted All' })
    },
  ).catch((err) => res.status(400).json(err))

  static delete = (req, res) => {
    const { id } = req.params

    return Admin.destroy({
      where: { id },
    }).then(
      (admin) => {
        if (!admin) return res.status(400).json({ msg: 'ID Not Found' })

        return res.status(200).json({ msg: 'Deleted' })
      },
    ).catch((err) => res.status(400).json(err))
  }
}

export default AdminController
