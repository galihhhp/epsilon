import { Room } from '../../models'

class GameController {
  static getGame = (req, res) => {
    const { roomNumber } = req.params

    return Room.findOne({ where: { roomNumber } })
      .then(
        (room) => {
          if (!room) return res.status(400).json({ message: 'not exist' })

          return res.status(200).json({ room })
        },
      ).catch((e) => res.status(500).json({ message: 'Internal Server Error' }))
  }

  static getHistory = (req, res) => {
    res.status(200).json(data)
  }

  static getHistoryView = (req, res) => res.render(
    'posts/history.ejs', { results: data },
  )

  static getCompChoice = (req, res) => {
    let compChoice = ''
    const choice = Math.random();
    if (choice <= 1 / 3) compChoice = 'rock';
    if (choice > 1 / 3 && choice <= 2 / 3) compChoice = 'paper';
    if (choice > 2 / 3) compChoice = 'scissor';

    res.send(compChoice)
  }

  static getIndexView = (req, res) => {
    res.render('posts/index')
  }
}

export default GameController
