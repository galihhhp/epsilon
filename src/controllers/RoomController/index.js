import { Op } from 'sequelize'
import { User, Room } from '../../models'

class RoomController {
  static joinRoom = (req, res) => {
    const { roomNumber } = req.params
    const { secondPlayer } = req.body

    const checkROom = Room.findOne({
      where: {
        [Op.and]: [
          { roomNumber },
          {
            secondPlayer: {
              [Op.is]: null,
            },
          },
        ],
      },
    })

    if (!checkROom) {
      return res.status(400).json({ msg: 'full' })
    }

    return Room.findOne({
      where: { roomNumber },
    }).then((room) => {
      room.update({ secondPlayer })
        .then(() => res.status(200).json({ roomNumber }))
        .catch((err) => res.status(500).json({ msg: err }))
    }).catch((err) => res.status(500).json({ msg: err }))
  }

  static get = (req, res) => Room.findAll().then(
    (rooms) => res.status(200).json(rooms),
  ).catch(
    (err) => res.status(400).json({ msg: err }),
  )

  static getById = (req, res) => {
    const { id } = req.params

    Room.findOne({
      where: { id },
    })
      .then((room) => res.status(200).json(room))
      .catch((err) => res.status(400).json({ msg: err }))
  }

  static create = (req, res) => {
    const { roomNumber, firstPlayer } = req.body

    Room.create({
      roomNumber,
      firstPlayer,
    })
      .then((room) => res.status(200).json({ roomNumber: room.id }))
      .catch((err) => res.status(400).json({ msg: err }))
  }
}

export default RoomController
