import bcrypt from 'bcrypt'
import { User } from '../../models'
import { JwtAuth } from '../../middlewares/Auth'

class UserController {
  static login = (req, res) => {
    const { username, password } = req.body

    return User.findOne({
      where: {
        username,
      },
    }).then(
      (user) => {
        if (user && bcrypt.compareSync(password, user.password)) {
          const { id, email, fullName } = user

          return JwtAuth.sign({ id, email, fullName },
            (token) => res.status(200).cookie('token', token, { httpOnly: true }).json({ token }).redirect('/game'))
        }

        return res.status(401).json({ message: 'Unauthorized' })
      },
    ).catch(
      () => res.status(500).json({ message: 'Internal Server Error' }),
    )
  }

  static register = (req, res) => {
    const {
      username,
      password,
      confirmPassword,
      email,
      fullName,
    } = req.body

    if (password !== confirmPassword) {
      res.status(404).json({ msg: 'please, check your password again' })
    } else {
      const hashPassword = User.hash(password)

      User.create({
        username,
        password: hashPassword,
        email,
        fullName,
      })
        .then((user) => res.status(200).json(user))
        .catch((err) => res.status(500).json(err))
    }
  }

  static get = (req, res) => User.findAll().then(
    (users) => res.status(200).json(users),
  ).catch(
    (err) => res.status(400).json({ msg: err }),
  )

  static getById = (req, res) => {
    const { id } = req.params

    User.findOne({
      where: { id },
    })
      .then((user) => res.status(200).json(user))
      .catch((err) => res.status(400).json({ msg: err }))
  }

  static update = (req, res) => {
    const { id } = req.params
    const {
      username,
      password,
      email,
      fullName,
    } = req.body

    User.update({
      username,
      password,
      email,
      fullName,
    }, {
      where: { id },
    })
      .then(() => res.json({ msg: 'SUCCES!' }))
      .catch((err) => res.status(400).json(err))
  }

  static deleteAll = (req, res) => User.destroy({
    truncate: true,
    cascade: true,
  }).then(
    (user) => {
      if (user) return res.status(400).json({ msg: 'Not Found' })

      return res.status(200).json({ msg: 'Deleted All' })
    },
  ).catch((err) => res.status(400).json(err))

  static delete = (req, res) => {
    const { id } = req.params

    return User.destroy({
      where: { id },
    }).then(
      (user) => {
        if (!user) return res.status(400).json({ msg: 'Not Found' })

        return res.status(200).json({ msg: 'Deleted' })
      },
    ).catch((err) => res.status(400).json(err))
  }
}

export default UserController
