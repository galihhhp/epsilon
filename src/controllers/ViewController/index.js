class ViewController {
  static getHomeView = (req, res) => {
    res.render('homepage')
  }

  static getCreateRoomView = (req, res) => {
    res.render('roomCreate')
  }

  static getJoinRoomView = (req, res) => {
    res.render('roomJoin')
  }

  static getDashboardView = (req, res) => {
    res.render('showDashboard')
  }

  static getAdminView = (req, res) => {
    res.render('showAdmin')
  }

  static getHistoryView = (req, res) => {
    res.render('showHistory')
  }

  static getAdminLoginView = (req, res) => {
    res.render('loginAdmin')
  }

  static getPlayerLoginView = (req, res) => {
    res.render('loginUser')
  }

  static getSignUpFormsView = (req, res) => {
    res.render('formSignUpUser')
  }

  static getAddAdminFormsView = (req, res) => {
    res.render('formAddAdmin')
  }

  static getUserFormsView = (req, res) => {
    res.render('formUser')
  }

  static getEditUserFormsView = (req, res) => {
    res.render('formEditUser')
  }
}

export default ViewController
