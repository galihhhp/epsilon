import jwt from 'jsonwebtoken'
import express from 'express'
import cookieParser from 'cookie-parser'

const app = express()
app.use(cookieParser())

export const BasicAuth = (req, res, next) => {
  const { headers: { authorization } } = req
  // Basic hasgtdiusadhss=  ==> ['Basic', 'hasgtdiusadhss=']

  const b64auth = (authorization || '').split(' ')[1] || ''
  const [username, password] = Buffer.from(b64auth, 'base64').toString().split(':')
  // username:password => ['username', 'password']

  // 123 => bcrypt => jsadosa798jkasjd98a

  console.log(username, password)
  // TODO: Validate User

  return next()
}

export class JwtAuth {
  static DEFAULT_OPTIONS = {
    algorithm: 'HS256',
    expiresIn: 3600,
  }

  static sign = (payload, success, error = () => {}) => jwt.sign(
    payload,
    process.env.JWT_SECRET,
    this.DEFAULT_OPTIONS,
    (err, token) => {
      if (err) return error(err)

      return success(token)
    },
  )

  static verify = (token, success, error = () => {}) => jwt.verify(
    token,
    process.env.JWT_SECRET,
    (err, decoded) => {
      if (err) return error(err)

      return success(decoded)
    },
  )
}

export const AuthMiddleware = (req, res, next) => {
  // const { token } = req.cookies.token
  const { cookies: { token = null } } = req

  if (!token) return res.status(401).json({ message: 'Unauthorized' })

  return JwtAuth.verify(
    token,
    (decoded) => {
      req.decoded = decoded

      return next()
    },
    () => res.status(401).json({ message: 'Unauthorized' }),
  )
}
