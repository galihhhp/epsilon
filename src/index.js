import express from 'express';
import path from 'path';
import swaggerUi from 'swagger-ui-express';
import yaml from 'yamljs';
import cookieparser from 'cookie-parser';
import cors from 'cors';
import apis from './routes/apis';
import views from './routes/views';

require('dotenv').config();

const app = express();
app.use(express.json());
app.use(express.static('public'));

app.use(cors());
app.use(cookieparser());

app.set('view engine', 'ejs');
app.set('views', 'src/views');

app.use('/', views);
app.use('/apis', apis);
app.use((req, res) => res.status(404).json({ 404: 'Not Found' }));

const swaggerDoc = yaml.load(path.resolve(__dirname, '../swagger.yaml'));
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDoc));

app.listen(3001, () => console.log('Server Up'));
