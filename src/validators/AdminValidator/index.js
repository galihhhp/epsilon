import { body, param } from 'express-validator'

class AdminValidator {
  static login = [
    body('username').exists().isString(),
    body('password').exists().isString(),
  ]

  static register = [
    body('username').exists().isString(),
    body('password').exists().isString(),
  ]

  static update = [
    param('id').exists().isUUID(),
    body('username').optional().isString(),
    body('password').optional().isString(),
  ]

  static delete = [
    param('id').exists().isUUID(),
  ]
}

export default AdminValidator
