import { body, param } from 'express-validator'

class UserValidator {
  static login = [
    body('username').exists().isString(),
    body('password').exists().isString(),
  ]

  static register = [
    body('username').exists().isString(),
    body('password').exists().isString(),
    body('email').exists().isString(),
    body('fullName').exists().isString(),
  ]

  static update = [
    param('id').exists().isUUID(),
    body('username').optional().isString(),
    body('password').optional().isString(),
    body('email').optional().isString(),
    body('fullName').optional().isString(),
  ]

  static delete = [
    param('id').exists().isUUID(),
  ]
}

export default UserValidator
