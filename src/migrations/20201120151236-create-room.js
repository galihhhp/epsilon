module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Room', {
      roomNumber: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        unique: true,
        allowNull: false,
      },
      firstPlayer: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false,
      },
      secondPlayer: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: true,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Room');
  },
};
