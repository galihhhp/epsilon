import express from 'express'
// import Middleware from '../../middlewares'
import ViewController from '../../controllers/ViewController'
import GameController from '../../controllers/GameController'
import AdminController from '../../controllers/AdminController'

const router = express.Router()

router.get('/', ViewController.getHomeView)
router.get('/dashboard', ViewController.getDashboardView)
router.get('/admin', ViewController.getAdminView)
router.get('/history', ViewController.getHistoryView)

// LOGIN
router.get('/login/admin', ViewController.getAdminLoginView)
router.get('/login/player', ViewController.getPlayerLoginView)

// FORMS
router.get('/createUser', ViewController.getUserFormsView)
router.get('/editUser', ViewController.getEditUserFormsView)
router.get('/signup', ViewController.getSignUpFormsView)
router.get('/add/admin', ViewController.getAddAdminFormsView)

// ROOM
router.get('/joinRoom', ViewController.getJoinRoomView)
router.get('/createRoom', ViewController.getCreateRoomView)

// GAME
router.get('/game', GameController.getIndexView)
router.get('/game/history', GameController.getHistoryView)

export default router
