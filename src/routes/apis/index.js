import express from 'express'
import Middleware from '../../middlewares'
import UserController from '../../controllers/UserController'
import HistoryController from '../../controllers/HistoryController'
import GameController from '../../controllers/GameController'
import AdminController from '../../controllers/AdminController'
import RoomController from '../../controllers/RoomController'
import AdminValidator from '../../validators/AdminValidator'
import UserValidator from '../../validators/UserValidator'

const router = express.Router()

router.get('/users', [Middleware.Auth], UserController.get)
router.get('/users/:id', [Middleware.Auth], UserController.getById)
router.post('/register/user', [UserValidator.register, Middleware.Auth], UserController.register)
router.patch('/users/:id', [Middleware.Auth], UserController.update)
router.delete('/users/:id', [Middleware.Auth], UserController.delete)
router.delete('/users/del/all', [Middleware.Auth], UserController.deleteAll)

router.get('/histories', [Middleware.Auth], HistoryController.get)
router.get('/histories/:id', [Middleware.Auth], HistoryController.getById)
router.post('/histories/:id', [Middleware.Auth], HistoryController.create)
router.patch('/histories/:id', [Middleware.Auth], HistoryController.update)
router.delete('/histories/:id', [Middleware.Auth], HistoryController.delete)

router.get('/admin', [Middleware.Auth], AdminController.get)
router.get('/admin/:id', [Middleware.Auth], AdminController.getById)
router.post('/register/admin', [AdminValidator.register, Middleware.Auth], AdminController.register)
router.patch('/admin/:id', [Middleware.Auth], AdminController.update)
router.delete('/admin/del/all', [Middleware.Auth], AdminController.deleteAll)
router.delete('/admin/:id', [Middleware.Auth], AdminController.delete)

router.get('/room', RoomController.get)
router.get('/room/:id', RoomController.getById)
router.post('/room', [Middleware.Auth], RoomController.create)
router.patch('/room/:id', [Middleware.Auth], RoomController.joinRoom)

router.post('/login/admin', [AdminValidator.login, Middleware.Guest], AdminController.login)
router.post('/login/player', [UserValidator.login, Middleware.Guest], UserController.login)

router.get('/compChoice', GameController.getCompChoice)
router.get('/game', GameController.getGame)

export default router
