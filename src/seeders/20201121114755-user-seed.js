const bcrypt = require('bcrypt')

const salt = bcrypt.genSaltSync()
const password = bcrypt.hashSync('password', salt)

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('User', [{
      id: '1192ce02-3bc5-4308-bdb7-4ba4c43f327c',
      username: 'galihhhp',
      password,
      email: 'galihhhp@email.com',
      fullName: 'Pangestu Galih',
    },
    {
      id: '17d8679f-b7ad-429b-9a87-d8a5925c4e32',
      username: 'kolamsusu',
      password,
      email: 'kolamsusup@email.com',
      fullName: 'Kolam Susu',
    },
    {
      id: 'a87e02b4-f63f-41a9-8beb-9ef0db23d7ae',
      username: 'peekaboo',
      password,
      email: 'peekaboo@email.com',
      fullName: 'Peek A Boo',
    },
    {
      id: '3806b999-1607-4b88-8dfe-e0ade844ae98',
      username: 'yakali',
      password,
      email: 'yakali@email.com',
      fullName: 'Yakali',
    },
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('User', null, {});
  },
};
