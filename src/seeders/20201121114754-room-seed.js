module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Room', [{
      roomNumber: 1234,
      firstPlayer: 'galihhhp',
      secondPlayer: 'yakali',
    },
    {
      roomNumber: 5678,
      firstPlayer: 'kolamsusu',
      secondPlayer: 'peekaboo',
    },
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Room', null, {});
  },
};
