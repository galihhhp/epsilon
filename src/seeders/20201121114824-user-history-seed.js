module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('UserHistory', [{
      id: '7bea761d-dec9-4150-aa75-ffeeaf4605b2',
      playerChoice: 'ROCK',
      opponentChoice: 'ROCK',
      result: 'DRAW',
      userId: '1192ce02-3bc5-4308-bdb7-4ba4c43f327c',
    },
    {
      id: '33fabd06-e259-4fef-a717-a2e7f3e1d27a',
      playerChoice: 'ROCK',
      opponentChoice: 'ROCK',
      result: 'DRAW',
      userId: '17d8679f-b7ad-429b-9a87-d8a5925c4e32',
    },
    {
      id: '4071a478-19df-4091-894b-b2388dbdd613',
      playerChoice: 'PAPER',
      opponentChoice: 'SCISSOR',
      result: 'LOOSE',
      userId: 'a87e02b4-f63f-41a9-8beb-9ef0db23d7ae',
    },
    {
      id: '72972c79-d4ff-4aa7-a5e5-88f324dcdbe9',
      playerChoice: 'SCISSOR',
      opponentChoice: 'PAPER',
      result: 'WIN',
      userId: '3806b999-1607-4b88-8dfe-e0ade844ae98',
    },
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('UserHistory', null, {});
  },
};
