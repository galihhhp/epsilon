/* eslint-disable no-shadow */
/* eslint-disable no-restricted-globals */
/* eslint-disable eol-last */
/* eslint-disable no-undef */
const toJoinRoom = () => {
  window.location.replace('http://localhost:3001/joinRoom')
}

const loginBtn = document.getElementById('loginUser')
const username = document.getElementById('usernameForm')
const password = document.getElementById('passwordForm')

loginBtn.addEventListener('click', (e) => {
  e.preventDefault()

  const apis = 'http://localhost:3001/apis/login/player'

  const headers = new Headers()
  headers.append('Content-Type', 'Application/json')

  const data = {
    username: username.value,
    password: password.value,
  }

  const user = JSON.stringify(data)

  const options = {
    method: 'POST',
    headers: headers,
    body: user,
  }

  fetch(apis, options)
    .then((res) => res.json())
    .then((result) => {
      if (result.message === 'Unauthorized') {
        alert('Unauthorized')
      }
      if (result.message === 'Not Found') {
        alert('Not Found')
      }

      console.log(result);
      toJoinRoom()
    })
    // .then(() => res.json(JSON.stringify(data)))
    // .then(toDashboard())
    .catch((err) => { throw err })
})