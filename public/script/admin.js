const reset = document.getElementById('reset')
const listAdmin = document.getElementById('listAdmin')

reset.addEventListener('click', () => {
  const apis = 'http://localhost:3001/apis/admin/del/all'
  const options = {
    method: 'DELETE',
  }
  fetch(apis, options)
    .then((res) => res.json())
    .catch((err) => { throw err })
})

removeAdmin = (adminId) => {
  const xhr = new XMLHttpRequest();

  xhr.onload = function() {
    getAdmin()
  }

  xhr.onerror = function() {
    alert('Internal Server Error')
  }

  xhr.open('DELETE', `http://localhost:3001/apis/admin/${adminId}`)
  xhr.send()
}

const renderAdmin = (admins) => {
  listAdmin.innerHTML = ''

  admins.forEach((admins) => {
    listAdmin.innerHTML += `
    <tr>
      <td><p class="text-center">${admins.id}</p></td>
      <td><p class="text-center">${admins.username}</p></td>
      <td><p class="text-center">${admins.password}</p></td>
      <td>
        <p class="text-center">
          <button id="${admins.id}" type="button" class="btn btn-success edit-btn">
            Edit
          </button>
        </p>
      </td>
      <td>
        <p class="text-center">
        <button id="${admins.id}" type="button" class="btn btn-danger delete-btn">
          Delete
        </button>
        </p>
      </td>
  </tr>`
  })

  const deleteBtn = document.querySelectorAll('.delete-btn')
  deleteBtn.forEach((del) => {
    del.addEventListener('click', (btn) => {
      const userId = btn.target.id
      removeAdmin(userId)
    })
  })
}

const getAdmin = () => {
  const xhr = new XMLHttpRequest();
  xhr.onload = function() {
    const responseJson = JSON.parse(this.responseText)
    if (responseJson.error) {
      console.log(responseJson.message)
    } else {
      renderAdmin(responseJson)
    }
  }
  xhr.onerror = function() {
    alert('Internal Server Error')
  }
  xhr.open('GET', 'http://localhost:3001/apis/admin')
  xhr.send()
}

getAdmin()