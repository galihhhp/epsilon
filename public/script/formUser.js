const username = document.getElementById('usernameForm')
const password = document.getElementById('passwordForm')
const confirmPassword = document.getElementById('confirmPasswordForm')
const email = document.getElementById('emailForm')
const fullName = document.getElementById('fullNameForm')
const button = document.getElementById('createUser')

const toDashboard = () => {
  location.replace('http://localhost:3001/dashboard')
}

button.addEventListener('click', () => {
  const xhr = new XMLHttpRequest();

  xhr.onload = function() {
    const responseJson = JSON.parse(this.responseText)
    if (responseJson.error) {
      console.log(responseJson.message)
    } else {
      toDashboard()
    }
  }

  xhr.onerror = function() {
    alert('Internal Server Error')
  }

  xhr.open('POST', 'http://localhost:3001/apis/register/user')
  xhr.setRequestHeader('Content-Type', 'application/json')
  const user = {
    username: username.value,
    password: password.value,
    confirmPassword: confirmPassword.value,
    email: email.value,
    fullName: fullName.value,
  }

  xhr.send(JSON.stringify(user));
})