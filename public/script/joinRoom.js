const inputRoom = document.getElementById('roomId')
const firstPlayer = document.getElementById('firstPlayer')
const createButton = document.getElementById('createRoom')

const toGame = () => {
  location.replace('http://localhost:3001/game')
}

createButton.addEventListener('click', () => {
  const xhr = new XMLHttpRequest();

  xhr.onload = function() {
    const responseJson = JSON.parse(this.responseText)
    if (responseJson.error) {
      console.log(responseJson.message)
    } else {
      alert('The Room Has Been Created')
      toGame()
    }
  }

  xhr.onerror = function() {
    alert('Internal Server Error')
  }

  xhr.open('POST', 'http://localhost:3001/apis/room')
  xhr.setRequestHeader('Content-Type', 'application/json')
  const room = {
    roomNumber: inputRoom.value,
    firstPlayer: firstPlayer.value,
  }

  xhr.send(JSON.stringify(room));
})