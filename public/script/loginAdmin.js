const toDashboard = () => {
  window.location.replace('http://localhost:3001/dashboard')
}

const loginBtn = document.getElementById('loginBtn')
const username = document.getElementById('usernameForm')
const password = document.getElementById('passwordForm')

loginBtn.addEventListener('click', (e) => {
  e.preventDefault()

  const apis = 'http://localhost:3001/apis/login/admin'

  const headers = new Headers()
  headers.append('Content-Type', 'Application/json')

  const data = {
    username: username.value,
    password: password.value,
  }

  const admin = JSON.stringify(data)

  const options = {
    method: 'POST',
    headers: headers,
    body: admin,
  }

  fetch(apis, options)
    .then((res) => res.json())
    .then((result) => {
      if (result.message === 'Unauthorized') {
        alert('Unauthorized')
      }
      if (result.message === 'Not Found') {
        alert('Not Found')
      }

      console.log(result);
      toDashboard()
    })
    // .then(() => res.json(JSON.stringify(data)))
    // .then(toDashboard())
    .catch((err) => { throw err })
})