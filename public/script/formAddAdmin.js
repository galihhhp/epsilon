/* eslint-disable eol-last */
/* eslint-disable space-before-function-paren */
/* eslint-disable func-names */
/* eslint-disable no-alert */
/* eslint-disable no-restricted-globals */
/* eslint-disable no-undef */
const username = document.getElementById('usernameForm')
const password = document.getElementById('passwordForm')
const confirmPassword = document.getElementById('confirmPasswordForm')
const button = document.getElementById('signUpBtn')

button.addEventListener('click', () => {
  const xhr = new XMLHttpRequest();

  xhr.onload = function() {
    const responseJson = JSON.parse(this.responseText)
    if (responseJson.error) {
      console.log(responseJson.message)
    } else {
      alert('Succesfully Add Admin')
    }
  }

  xhr.onerror = function() {
    alert('Internal Server Error')
  }

  xhr.open('POST', 'http://localhost:3001/apis/register/admin')
  xhr.setRequestHeader('Content-Type', 'application/json')
  const admin = {
    username: username.value,
    password: password.value,
    confirmPassword: confirmPassword.value,
  }

  xhr.send(JSON.stringify(admin));
})